.. _acknowledge:

==================
How to cite signac
==================

Please acknowledge the use of this software within the body of your publication for example by copying or adapting the following formulation:

*The computational workflow in general and data management in particular for this publication was primarily supported by the signac data management framework [1].*

  [1] C. S. Adorf, P. M. Dodd, V. Ramasubramani, and S. C. Glotzer. Simple data and workflow management with the signac framework. Comput. Mater. Sci., 146(C):220-229, 2018. DOI:10.1016/j.commatsci.2018.01.035.

A preprint of the paper published in the Journal of Computational Materials Science is available on the `arXiv: <https://arxiv.org/abs/1611.03543>`_.

To cite this referenc, you can use the following BibTeX entry:

.. code-block:: bibtex

    @article{signac_commat,
      author        = {Carl S. Adorf and
                       Paul M. Dodd and
                       Vyas Ramasubramani and
                       Sharon C. Glotzer},
      title         = {Simple data and workflow management with the signac framework},
      journal       = {Comput. Mater. Sci.},
      volume        = {146},
      number        = {C},
      year          = {2018},
      pages         = {220-229},
      doi           = {10.1016/j.commatsci.2018.01.035}
    }
