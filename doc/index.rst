.. signac-flow documentation master file, created by
   sphinx-quickstart on Wed Mar 16 14:21:08 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to signac-flow's documentation!
=======================================

.. image:: https://anaconda.org/conda-forge/signac-flow/badges/version.svg
   :target: https://anaconda.org/conda-forge/signac-flow

.. image:: https://img.shields.io/pypi/v/signac-flow.svg
   :target: https://img.shields.io/pypi/v/signac-flow.svg

The signac-flow tool provides the basic components to setup simple to complex workflows for `signac projects <https://glotzerlab.engin.umich.edu/signac>`_.
That includes the definition of data pipelines, execution of data space operations and the submission of operations to high-performance super computers.

The implementation is in pure Python, requires signac_ and is tested for Python versions 2.7 and 3.4+.

.. _signac: https://glotzerlab.engin.umich.edu/signac

The screencast below demonstrates the general concept of setting up a workflow with **signac-flow**.
For a detailed introduction, please checkout the :ref:`reference` documentation!

.. raw:: html

    <div align="center">
      <script type="text/javascript" src="https://asciinema.org/a/6uyqoqk87w1r5y0k09zj43ibp.js" id="asciicast-6uyqoqk87w1r5y0k09zj43ibp" async></script>
    </div>

.. toctree::
   :maxdepth: 2
   :hidden:

   installation
   reference
   supported_environments
   acknowledge
   flow

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

