API
===

Module contents
---------------

.. automodule:: flow
    :members:
    :show-inheritance:

flow.scheduler module
---------------------

.. automodule:: flow.scheduler
    :members:
    :show-inheritance:

flow.environment module
-----------------------

.. automodule:: flow.environment
    :members:
    :show-inheritance:

flow.environments module
------------------------

.. automodule:: flow.environments
    :members:

flow.manage module
------------------

.. automodule:: flow.manage
    :members:
    :show-inheritance:

flow.errors module
------------------

.. automodule:: flow.errors
    :members:
    :show-inheritance:

flow.fakescheduler module
-------------------------

.. automodule:: flow.fakescheduler
    :members:
    :show-inheritance:

flow.torque module
------------------

.. automodule:: flow.torque
    :members:
    :show-inheritance:

flow.slurm module
-----------------

.. automodule:: flow.slurm
    :members:
    :show-inheritance:
